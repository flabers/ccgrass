<div class="topmain">
    <div id="wrap" class="toppage-mobile">
		<div class="logo"><a href="<?php print $front_page; ?>"><img src="<?php print base_path().path_to_theme(); ?>/images/logo.png" /></a></div>
		<div class="topmenu">
            <a href="javascript:void(0)" class="topmenu-mobile-btn">
                <span class="mm-line"></span>
            </a>

			<?php print render($page['menu']); ?>
		</div>
    <div class="topmain-right">
        <div class="change-sity">
            <select name="sity" id="sity" class="change-sity__select">
                <option value="krivoy_rog">Кривой Рог</option>
                <option value="kyiv">Киев</option>
            </select>
        </div>

      <?php if(!isset($_COOKIE['sity'])){?>
        <div class="confirm-city">
          <p class="your_city">Ваш город — <strong>Киев?</strong></p>
          <div class="button-container">
            <span id="confirm-city-yes">Да</span>  <span id="confirm-city-no">Нет</span>
          </div>
        </div>
      <?php };?>
        <a href="#phonesWrap" class="hd-phone-icon fancyphones"></a>
        <div class="toptel" id="phonesWrap">
            <div class="hd-phone hd-phone_sity-change" data-sity="krivoy_rog">
                <a href="tel:0679119090" class="maintel"><span>(067)</span> 911-90-90</a>
                <div class="hovertel">
                    <a href="tel:0503217522"><span>(050)</span> 321-75-22</a>
                    <a href="tel:0564990299"><span>(056)</span> 499-02-99</a>
                    <div class="recall"><a href="#request" class="fancyform"><span>Перезвонить вам?</span></a></div>
                </div>
            </div>
            <div  class="hd-phone hd-phone_sity-change" data-sity="kyiv">
                <a href="tel:0981662686" class="maintel"><span>(098)</span> 166 26 86</a>
                <div class="hovertel">
                    <a href="tel:0443904441"><span>(044)</span> 390 44 41</a>
                    <div class="recall"><a href="#request" class="fancyform"><span>Перезвонить вам?</span></a></div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clear"></div>
</div>
<div class="mainimg">
	<div id="wrap">
		<div class="mileft">
			<div class="miltop">Искуcственная трава</div>
			<div class="milcenter">С доставкой по всей Украине</div>
			<div class="milbot">Большой выбор. Быстрая доставка. Лучшие цены</div>
		</div>
		<div class="miright">
			<div class="miprice">
				<div class="ot">от</div>
				<div class="cost"><span>119</span>грн/м<sup>2</sup></div>
				<div class="clear"></div>
			</div>
			<div class="mibtn"><a href="#request" class="fancyform">Получить консультацию</a></div>
		</div>
	</div>
</div>
<div class="clear"></div>
<div id="wrap">
<div class="application">
	<div class="appttl">Применение искусственной травы</div>
	<div class="appdesc">
		<div class="apdleft">
			<a href="/sport-grass" class="apdleftl">&nbsp;</a>
			<div class="aplttl"><a href="/sport-grass">Трава для спорта</a></div>
			<div class="apltxt">Искусственный газон &mdash; популярное решение для многих спортивных арен различных видов спорта. Материал не подвержен влиянию погодных условий, пожароустойчивый, прост в уходе, безопасен для людей, а его внешний вид, практически, не отличается от обычного газона.
			<div class="apmore"><a href="/sport-grass"><span>Подробнее</span></a></div>
			</div>
		</div>
		<div class="apdright">
			<a href="/landscape-grass" class="apdrightl">&nbsp;</a>
			<div class="aplttl"><a href="/landscape-grass">Ландшафтная трава</a></div>
			<div class="apltxt">Искусственная трава обладает прекрасными эксплуатационными характеристиками и практически неотличима от натурального газона. Устойчивость к любым погодным условиям, универсальность и практичность делают этот материал просто незаменимым.
			<div class="apmore"><a href="/landscape-grass"><span>Подробнее</span></a></div>
			</div>
		</div>

	</div>
</div>
<div class="clear"></div>
<div class="advantages">
	<div class="adttl">Преимущества травы CCGrass</div>
	<div class="adlist">
        <div class="adlist__list">
            <div class="adlist__item">
                <div class="adlist__block natural"><div class="adltxt">натуральный <br />вид</div></div>
            </div>
            <div class="adlist__item">
                <div class="adlist__block poliv"><div class="adltxt">не нужно <br />поливать</div></div>
            </div>
            <div class="adlist__item">
                <div class="adlist__block green"><div class="adltxt">зеленая <br />круглый год</div></div>
            </div>
            <div class="adlist__item">
                <div class="adlist__block care"><div class="adltxt">не требует <br />ухода</div></div>
            </div>
        </div>
	</div>
</div>
<div class="clear"></div>
<div class="present">
	<div class="prtxt">
		<div class="prttl">CCGrass — лидер на рынке изготовления искусственной травы</div>
		<div class="prdesc">
		<p>Мы предлагаем искусственную траву высочайшего качества с доставкой по всей территории Украины. Наша компания уже долгие годы занимает лидирующие позиции на рынке. Благодаря современным технологиям ландшафтная трава и газоны для спортивных площадок, практически, неотличимы от натуральной травы. Продукция отвечает всем международным требованиям качества и безопасности, производитель имеет ряд международных сертификатов и постоянно совершенствует продукцию.</p>
		</div>
	</div>
	<div class="clear"></div>
</div>
</div>
<div id="form">
	<div id="wrap">
		<div class="formttl"><a href="#sample" class="fancyform">Заказать бесплатные образцы</a></div>
		<div class="formdiv">
			<div class="inputs" id="orazform">
				<form id="orazets">
				<input name="name" class="nameinput" placeholder="Введите свое имя" />
				<input name="phone" class="phoneinput" placeholder="Контактный телефон" id="phone" />
				<input type="submit" value="Перезвонить" class="btn" />
				</form>
			</div>
			<div id="orazresult">
				Ваша заявка успешно отправлена! Ождайте нашего звонка.
			</div>
			<div class="formtxt">или звоните</div>
            <div class="formphone formphone-sity-change" data-sity="krivoy_rog"><a href="tel:0679119090">(067) 911-90-90</a>,&nbsp;<a href="tel:0503217522">(050) 321-75-22</a></div>
            <div class="formphone formphone-sity-change" data-sity="kyiv"><a href="tel:0443904441">(044) 390 44 41</a>,&nbsp;<a href="tel:0981662686">(098) 166 26 86</a></div>
		</div>
	</div>
</div>
<div class="clear"></div>
<div id="wrap">
	<?php print render($page['products']); ?>
	<div class="clear"></div>
	<div class="spectxt">
	<h1><?php print $title ?></h1>
	<?php print render($page['content']); ?>
	</div>
</div>
<div class="clear"></div>
<div id="footer">
	<div id="wrap">
		<div class="topfooter">
			<div class="fmenu">
				<?php print render($page['footermenu']); ?>
			</div>
            <div class="fphone fphone_sity-change" data-sity="krivoy_rog"><a href="tel:0679119090">(067) 911-90-90</a>,&nbsp;
                <a href="tel:0503217522">(050) 321-75-22</a>,&nbsp;<a href="tel:0564990299">(056) 499-02-99</a></div>
            <div class="fphone fphone_sity-change" data-sity="kyiv"><a href="tel:0443904441">(044) 390 44 41</a>,&nbsp;<a
                        href="tel:0981662686">(098) 166 26 86</a></div>
			<div class="clear"></div>
		</div>
		<div class="botfooter">
			<div class="sitename">CCGrass Украина (с) 2015-<?php print date('Y'); ?></div>
			<div class="socbtns"><a href="#"><img src="<?php print base_path().path_to_theme(); ?>/images/soc.png" alt="" /></a></div>
			<div class="copyright">Создание сайта - <a href="http://www.flabers.com.ua/" target="_blank">Интернет-агентство Флаберс</a></div>
		</div>
	</div>
</div>
<!-- Facebook Pixel Code -->
<script>
    !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
        n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
        document,'script','https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '346173409285559'); // Insert your pixel ID here.
    fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
               src="https://www.facebook.com/tr?id=346173409285559&ev=PageView&noscript=1"
    /></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script>

    $("#confirm-city-yes").click( function () {
        document.cookie = "sity=kyiv";
        $(".confirm-city").hide();
        location.reload();
    });
    $("#confirm-city-no").click( function () {
        document.cookie = "sity=krivoy_rog";
        $(".confirm-city").hide();
        location.reload();
    });
</script>
