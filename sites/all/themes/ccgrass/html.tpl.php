<?php

/**
 * @file
 * Default theme implementation to display the basic html structure of a single
 * Drupal page.
 *
 * Variables:
 * - $css: An array of CSS files for the current page.
 * - $language: (object) The language the site is being displayed in.
 *   $language->language contains its textual representation.
 *   $language->dir contains the language direction. It will either be 'ltr' or 'rtl'.
 * - $rdf_namespaces: All the RDF namespace prefixes used in the HTML document.
 * - $grddl_profile: A GRDDL profile allowing agents to extract the RDF data.
 * - $head_title: A modified version of the page title, for use in the TITLE
 *   tag.
 * - $head_title_array: (array) An associative array containing the string parts
 *   that were used to generate the $head_title variable, already prepared to be
 *   output as TITLE tag. The key/value pairs may contain one or more of the
 *   following, depending on conditions:
 *   - title: The title of the current page, if any.
 *   - name: The name of the site.
 *   - slogan: The slogan of the site, if any, and if there is no title.
 * - $head: Markup for the HEAD section (including meta tags, keyword tags, and
 *   so on).
 * - $styles: Style tags necessary to import all CSS files for the page.
 * - $scripts: Script tags necessary to load the JavaScript files and settings
 *   for the page.
 * - $page_top: Initial markup from any modules that have altered the
 *   page. This variable should always be output first, before all other dynamic
 *   content.
 * - $page: The rendered page content.
 * - $page_bottom: Final closing markup from any modules that have altered the
 *   page. This variable should always be output last, after all other dynamic
 *   content.
 * - $classes String of classes that can be used to style contextually through
 *   CSS.
 *
 * @see template_preprocess()
 * @see template_preprocess_html()
 * @see template_process()
 *
 * @ingroup themeable
 */
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN"
  "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language; ?>" version="XHTML+RDFa 1.0"
      dir="<?php print $language->dir; ?>"<?php print $rdf_namespaces; ?>>

<head profile="<?php print $grddl_profile; ?>">
  <?php print $head; ?>
  <title><?php print $head_title; ?></title>
  <?php print $styles; ?>
  <?php print $scripts; ?>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!--  <meta name="viewport" content="width=1170px">-->
  <!-- Google Tag Manager -->
  <script>(function (w, d, s, l, i) {
      w[l] = w[l] || [];
      w[l].push({
        'gtm.start':
          new Date().getTime(), event: 'gtm.js'
      });
      var f = d.getElementsByTagName(s)[0],
        j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
      j.async = true;
      j.src =
        'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
      f.parentNode.insertBefore(j, f);
    })(window, document, 'script', 'dataLayer', 'GTM-KT2ST2H');</script>
  <!-- End Google Tag Manager -->


</head>
<body class="<?php print $classes; ?>" <?php print $attributes; ?>>
<!-- Google Tag Manager (noscript) -->
<noscript>
  <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KT2ST2H"
          height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->
<div id="skip-link">
  <a href="#main-content" class="element-invisible element-focusable"><?php print t('Skip to main content'); ?></a>
</div>
<?php print $page_top; ?>
<?php print $page; ?>
<?php print $page_bottom; ?>

<div id="request" class="formavopros-wrap" style="display:none">
  <div id="before_click" class="before_click">
    <p class="popup-form-title">Оформить заявку</p>
    <form id="formavopros">
      <div class="in"><input type="text" name="name" placeholder="Ваше имя" class="intxt"/></div>
      <div class="in"><input type="text" name="phone" placeholder="Контактный номер телефона" id="phone2"
                             class="intxt"/></div>
      <div class="in"><textarea name="vopros" placeholder="Укажите детали заявки" class="intxt"></textarea></div>
      <div class="in"><input type="submit" value="Отправить заявку" class="sbtn"/></div>
      <div class="clear twenty"></div>
    </form>
  </div>
  <div id="after_click" class="after_click" style="display:none;">
    <p class="resttl">Спасибо за ваше обращение!</p>
    <p>Мы свяжемся с вами в ближайшее время.</p>
  </div>
</div>

<div id="sample" class="sample-wrap" style="display:none; width:600px;">
  <div id="sample_click" class="sample_click">
    <p class="popop-title">Получить образец продукции</p>
    <form id="sampleforma">
      <div class="in">
        <p><strong>Образец какой продукции вы хотите получить?</strong></p>
        <?php
        foreach (getSamples() as $samples) {
          print  '<h4 class="sample-title">' . $samples['title'] . '</h4>';
          foreach ($samples['values'] as $sample) {
            print  '<div class="chekbox"><input type="checkbox" name="samples[]" value="' . $sample['name'] . '"' .  ((arg(1) == $sample['entity_id']) ? 'checked': '') .'/>' . $sample['name'] . '</div>';
          }
        }
        ?>
      </div>
      <div class="in"><p><strong>Заполните ваши данные</strong></p></div>
      <div class="in"><input type="text" name="name" placeholder="ФИО" class="intxt"/></div>
      <div class="in"><input type="text" name="phone" placeholder="Контактный номер телефона" id="phone3"
                             class="intxt"/></div>
      <!-- Доставка START	-->
      <div class="in" id="delpay2">
        <p><strong>Доставка</strong></p>
        <div>
          <input type="radio" name="delivery" value="novaposhta">
          <span>Новая почта</span>
          <div data-delpay="novaposhta" style="display: none">
            <div>
              <input type="text" name="delivery_city" class="intxt" placeholder="Город">
            </div>
            <div>
              <input type="text" name="delivery_address" class="intxt" placeholder="Адрес или Номер отделения">
            </div>
          </div>
        </div>
        <div>
          <input type="radio" name="delivery" value="pickup">
          <span>Самовывоз</span>
          <div data-delpay="pickup" style="display: none">
            г.Кривой Рог, ул.Лётчиков 9 (бывший Атлант-Сервис)
          </div>
        </div>
      </div>
      <!-- Доставка  END	-->
      <!--	<div class="in"><input type="text" name="np" placeholder="№ склада" class="intxt" style="width:80px;" maxlength="3" /> Новой Почты</div>-->
      <div class="in"><textarea name="vopros" placeholder="Укажите детали заявки" class="intxt"></textarea></div>
      <div class="in"><input type="submit" value="Отправить заявку" class="sbtn"/></div>
      <div class="clear twenty"></div>
    </form>
  </div>
  <div id="sampleafter_click" class="sampleafter_click" style="display:none;">
    <p class="resttl">Спасибо за ваше обращение!</p>
    <p>Мы свяжемся с вами в ближайшее время.</p>
  </div>
</div>

<style>
  #bingc-phone-button svg.bingc-phone-button-circle circle.bingc-phone-button-circle-inside {
    fill: #FFE836 !important;
  }

  #bingc-phone-button:hover svg.bingc-phone-button-circle
  circle.bingc-phone-button-circle-inside {
    fill: #FFE836 !important;
  }

  #bingc-phone-button div.bingc-phone-button-tooltip {
    background: #FFE836 !important;
  }

  #bingc-phone-button div.bingc-phone-button-tooltip svg.bingc-phone-button-arrow
  polyline {
    fill: #FFE836 !important;
  }

  #bingc-phone-button div.bingc-phone-button-icon-text span {
    color: #212121 !important;
  }

  #bingc-phone-button svg.bingc-phone-button-icon-icon path {
    fill: #212121 !important;
  }

  #bingc-passive div.bingc-passive-overlay div.bingc-passive-content div.bingc-passive-get-phone-form form.bingc-passive-get-phone-form a.bingc-passive-phone-form-button {
    background: #FFE836 !important;
  }
</style>

</body>
</html>
