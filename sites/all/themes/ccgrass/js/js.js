(function ($) {

  $(document).ready(function () {

    $('#travaorder select').niceSelect();


    $('.fancybox').fancybox();


    $('.fancyform').fancybox({

      padding: 0

    });


    $(".fancyvideo").fancybox({

      maxWidth: 900,

      maxHeight: 506,

      fitToView: false,

      width: '70%',

      height: '70%',

      autoSize: false,

      closeClick: false,

      openEffect: 'none',

      closeEffect: 'none'

    });


    $('#phone').inputmask("mask", {
      "mask": "(999) 999-99-99"
    });

    $('#phone2').inputmask("mask", {
      "mask": "(999) 999-99-99"
    });

    $('#phone3').inputmask("mask", {
      "mask": "(999) 999-99-99"
    });


    $('#length').numberMask({
      type: 'float',
      afterPoint: 1,
      defaultValueInput: '',
      decimalMark: '.'
    });


    $('#width').change(function () {

      var width = $('#travaorder .nice-select .current').text();

      width = parseInt(width);

      var lengthElem = $('#length'),
         selected = $("option:selected", this),
         length = parseFloat($(selected).attr('data-default-len')) || 1;

      lengthElem.val(length);

      var square = (width * length).toFixed(1);

      $('.areanum').text(square);

      var productPrice = parseFloat($('.pgcost span').text());

      productPrice = parseFloat(productPrice);

      var totalPrice = square * productPrice;

      var totalPrice = totalPrice.toFixed(1);

      $('.pricenum').text(totalPrice);


      $('#form-width').text(width);
      $('#form-width-input').val(width);

      $('#form-length').text(length);
      $('#form-length-input').val(length);

      $('#form-square').text(square);
      $('#form-square-input').val(square);

      $('#form-cost').text(totalPrice);
      $('#form-cost-input').val(totalPrice);

    });


    $('#length').keyup(function () {

      var width = $('#travaorder .nice-select .current').text();

      width = parseInt(width);


      var length = $(this).val();


      var square = (width * length).toFixed(1);

      $('.areanum').text(square);

      var productPrice = parseFloat($('.pgcost span').text());

      productPrice = parseFloat(productPrice);

      var totalPrice = square * productPrice;

      var totalPrice = totalPrice.toFixed(1);

      $('.pricenum').text(totalPrice);


      $('#form-width').text(width);
      $('#form-width-input').val(width);

      $('#form-length').text(length);
      $('#form-length-input').val(length);

      $('#form-square').text(square);
      $('#form-square-input').val(square);

      $('#form-cost').text(totalPrice);
      $('#form-cost-input').val(totalPrice);

    });

    if ($('.pgcost span').length) {

      $('#travaorder').show();

    } else {


      $('#travaorder').hide();

    }


    $('#order-bth').click(function () {

      if (parseInt($('.pricenum').text()) != 0) {
        $('#order-items').show();
      } else {
        $('#order-items').hide();

      }

    });

    $.validator.addMethod("regex", function (value, element, regexpr) {
      return regexpr.test(value);
    }, "Укажите корректный номер");


    $('#orazets').validate({

      rules: {

        name: {

          required: true,
          minlength: 3

        },

        phone: {

          required: true,
          regex: /^\(\d{3}\)\s\d{3}-\d{2}-\d{2}$/

        }

      },

      messages: {

        name: {

          required: "Обязательно",

          minlength: "Минимум 3 символа",

        },

        phone: {

          required: "Обязательно",

        }

      },

      success: function (element) {

        element.addClass('valid')

          .closest('.form-group').removeClass('error');

      },

      submitHandler: function (form) {

        $.ajax({

          dataType: 'html',

          type: 'post',

          url: '/mail.php',

          data: $(form).serialize(),

          success: function (responseData) {

            $('#orazform').remove();

            $('#orazresult').fadeIn(1000);

            dataLayer.push({
              'eventCategory': 'order',
              'eventAction': 'send',
              'event': 'event-to-ga'
            });

          },

          error: function (responseData) {

            console.log('Ajax request not recieved!');

          }

        });

        return false;

      }

    });


    $('#formavopros').validate({
      rules: {
        delivery: {
          required: true,
        },
        payment: {
          required: true,
        },
        delivery_city: {
          required: true,
          minlength: 3
        },
        delivery_address: {
          required: true,
          minlength: 1
        },
        name: {
          required: true,
          minlength: 3
        },
        phone: {
          required: true,
          regex: /^\(\d{3}\)\s\d{3}-\d{2}-\d{2}$/

        },
      },
      messages: {
        name: {
          required: "Обязательно",
          minlength: "Минимум 3 символа",
        },
        payment: {
          required: "Обязательно",
        },
        delivery: {
          required: "Обязательно",
        },
        delivery_city: {
          required: "Обязательно",
          minlength: "Минимум 3 символа",
        },
        delivery_address: {
          required: "Обязательно",
          minlength: "Минимум 1 символ",
        },
        phone: {
          required: "Обязательно",
        }
      },

      success: function (element) {

        element.addClass('valid')

          .closest('.form-group').removeClass('error');

      },

      submitHandler: function (form) {

        $.ajax({

          dataType: 'html',

          type: 'post',

          url: '/mail.php',

          data: $(form).serialize(),

          success: function (responseData) {

            var wrap = $(form).closest('.formavopros-wrap');

            wrap.find('.before_click').fadeOut(500, function () {
              wrap.find('.after_click').fadeIn(500);
            });


            dataLayer.push({
              'eventCategory': 'order',
              'eventAction': 'send',
              'event': 'event-to-ga'
            });

          },

          error: function (responseData) {

            console.log('Ajax request not recieved!');

          }

        });

        return false;

      }

    });


    $('#sampleforma').validate({
      rules: {
        name: {
          required: true,
          minlength: 3
        },
        delivery: {
          required: true,
        },
        delivery_city: {
          required: true,
          minlength: 3
        },
        delivery_address: {
          required: true,
          minlength: 1
        },
        phone: {
          required: true,
          regex: /^\(\d{3}\)\s\d{3}-\d{2}-\d{2}$/
        },
      },
      messages: {
        name: {
          required: "Обязательно",
          minlength: "Минимум 3 символа",
        },
        delivery: {
          required: "Обязательно",
        },
        delivery_city: {
          required: "Обязательно",
          minlength: "Минимум 3 символа",
        },
        delivery_address: {
          required: "Обязательно",
          minlength: "Минимум 1 символ",
        },
        phone: {
          required: "Обязательно",
        }

      },

      success: function (element) {

        element.addClass('valid')

          .closest('.form-group').removeClass('error');

      },

      submitHandler: function (form) {

        $.ajax({

          dataType: 'html',

          type: 'post',

          url: '/sample.php',

          data: $(form).serialize(),

          success: function (responseData) {

            var wrap = $(form).closest('.sample-wrap');

            wrap.find('.sample_click').fadeOut(500, function () {
              wrap.find('.sampleafter_click').fadeIn(500);
            });

            dataLayer.push({
              'eventCategory': 'order',
              'eventAction': 'send',
              'event': 'event-to-ga'
            });

          },

          error: function (responseData) {

            console.log('Ajax request not recieved!');

          }

        });

        return false;

      }

    });


  });


  var cookieFunc = {

    //метод получает значение куки по имени

    getCookie: function (name) {

      var matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
      ));

      return matches ? decodeURIComponent(matches[1]) : undefined;

    },

    //метод устанавливает новую или изменяет существующую куку

    setCookie: function (name, value, options) {

      options = options || {};


      var expires = options.expires;


      if (typeof expires == "number" && expires) {

        var d = new Date();

        d.setTime(d.getTime() + expires * 1000);

        expires = options.expires = d;

      }

      if (expires && expires.toUTCString) {

        options.expires = expires.toUTCString();

      }


      value = encodeURIComponent(value);

      var updatedCookie = name + "=" + value;


      for (var propName in options) {

        updatedCookie += "; " + propName;

        var propValue = options[propName];


        if (propValue !== true) {

          updatedCookie += "=" + propValue;

        }

      }


      document.cookie = updatedCookie;

    }


  }
  //объект изменяет данные на сайте в зависимости от значение куки

  var changeSityContent = (function () {


    return {

      init: function () {

        var _this = this;


        _this.changeSity();


        _this.changeInfo('.telblock .tel');

        _this.changeInfo('.adresblock .adres');

        _this.changeInfo('.fphone');

        _this.changeInfo('.formphone');

        _this.changeInfo('.hd-phone');


      },

      changeInfo: function (selector) {


        var items = $(selector)

        var activeItem;

        var cookie = cookieFunc.getCookie('sity');

        if (cookie) {

          activeItem = items.filter('[data-sity="' + cookie + '"]');

        } else {

          activeItem = items.filter('[data-sity="krivoy_rog"]');

        }

        activeItem.addClass('element-visible').siblings().removeClass('element-visible');


      },

      changeSity: function () {

        var _this = this;

        var sitiesWrapper = $('.change-sity');

        var sities = $('.change-sity .list .option');

        var cookie = cookieFunc.getCookie('sity');

        var selectCookie = sities.filter('[data-value="' + cookie + '"]');

        if (cookie) {

          selectCookie.trigger('click');

          $('.change-sity__select').removeClass('open')

        }

        sitiesWrapper.on('click', '.option', function () {


          var dataCookie = $(this).data('value');

          cookieFunc.setCookie('sity', dataCookie, {

            'path': '/',

            'expires': 10000000

          })

          _this.changeInfo('.telblock .tel');

          _this.changeInfo('.adresblock .adres');

          _this.changeInfo('.fphone');

          _this.changeInfo('.formphone');

          _this.changeInfo('.hd-phone');

        })


      }

    }

  }());

  var mobileInnerDropdown = (function () {

    var flag = true;
    var duration = 400;

    return {
      init: function () {
        if ($(window).width() < 993) {

          $('.topmenu-mobile-btn').on('click', function () {
            if (!$(this).hasClass('active')) {
              $(this).addClass('active')
              $('.toppage-mobile .region-menu').slideDown();
            } else {

              $(this).removeClass('active')
              $('.toppage-mobile .region-menu').slideUp();

            }
          })


          $('.topmenu').on('click', 'li > a, li > span', function (e) {
            var $this = $(this);
            var listItem = $(this).closest('li');
            var submenu = listItem.find('#producthover');
            // По клику на меню Продукция реализовать сразу переход на страницу
            if (false && submenu.length) {
              e.preventDefault();
              if (flag) {
                flag = false;
                if (!submenu.hasClass('open')) {
                  submenu.slideDown(duration, function () {
                    flag = true;
                  });
                  submenu.addClass('open');
                } else {
                  submenu.slideUp(duration, function () {
                    flag = true;
                  });
                  submenu.removeClass('open');
                }
              }

            }

          });

        }
      }
    }
  }());


  $(document).ready(function () {

    $('#sity').niceSelect();
    changeSityContent.init();
    mobileInnerDropdown.init();
    $('.fancyphones').fancybox();

    $('#width').change();

    formDeliveryPay('#delpay', '#formavopros');
    formDeliveryPay('#delpay2', '#sampleforma');
  })

  function formDeliveryPay(wrapper, form) {

    $(wrapper + ' > div > span').on('click', function (e) {
      var current = $(e.currentTarget);
      var prev = $(current).prev();
      if($(prev).prop("tagName") === 'INPUT'){
        $(prev).prop("checked", true).change();
      }
    });

    var
      validator = $(form).data('validator'),
      wrapper_selector = wrapper,
      payment = 'payment',
      delivery = 'delivery',
      event_selector = wrapper_selector + ' ' + formDeliveryPaySelector(payment) + ', ' +
        wrapper_selector + ' ' + formDeliveryPaySelector(delivery);

    $(event_selector).on('change', function (e) {

      var parent = $(this).closest(wrapper_selector),
        name = $(this).attr('name'),
        method = name + $(this).val();

      $(parent).find(formDeliveryPaySelector(delivery, 'novaposhta')).parent().show();

      if(name === payment){
        $(parent).find('[data-delpay="privat24"], [data-delpay="bank"], [data-delpay="nds"], [data-delpay="cash"]').hide();
      }

      switch (method) {
        case delivery + 'novaposhta':
          $(parent).find('[data-delpay="novaposhta"]').show();
          $(parent).find('[data-delpay="pickup"]').hide();
          if(validator) {
            validator.settings.rules.delivery_address.required = true;
            validator.settings.rules.delivery_city.required = true;
          }
          break;
        case delivery + 'pickup':
          $(parent).find('[data-delpay="pickup"]').show();
          $(parent).find('[data-delpay="novaposhta"]').hide();
          if(validator) {
             validator.settings.rules.delivery_address.required = false;
             validator.settings.rules.delivery_city.required = false;
          }
          break;
        case payment + 'bank':
          $(parent).find('[data-delpay="bank"]').show();
          break;
        case payment + 'nds':
          $(parent).find('[data-delpay="nds"]').show();
          break;
        case payment + 'privat24':
          $(parent).find('[data-delpay="privat24"]').show();
          break;
        case payment + 'cash':
          $(wrapper_selector + ' ' + formDeliveryPaySelector(delivery, 'pickup')).prop("checked", true).change();
          $(parent).find('[data-delpay="cash"]').show();
          $(parent).find('[data-delpay="privat24"]').hide();
          $(parent).find(formDeliveryPaySelector(delivery, 'novaposhta')).parent().hide();
          break;
      }
    });

    // Устаналиваем значения по умолчанию для доставки
    $(wrapper_selector + ' ' + formDeliveryPaySelector(delivery, 'novaposhta')).prop("checked", true).change();

    // Устаналиваем значения по умолчанию для оплаты
    $(wrapper_selector + ' ' + formDeliveryPaySelector(payment, 'privat24')).prop("checked", true).change();

  }

  function formDeliveryPaySelector(name, value) {
    var result = 'input[name="' + name + '"]';
    if (value)
      result += '[value="' + value + '"]';
    return result;
  }

  function moveSpinerCounter() {
    var width = $('#travaorder .nice-select .current').text();
    width = parseInt(width);
    var length = $('#length').val();
    var square = (width * length).toFixed(1);
    $('.areanum').text(square);
    var productPrice = parseFloat($('.pgcost span').text());
    productPrice = parseFloat(productPrice);
    var totalPrice = square * productPrice;
    totalPrice = totalPrice.toFixed(1);
    $('.pricenum').text(totalPrice);
    $('#form-width').text(width);
    $('#form-width-input').val(width);
    $('#form-length').text(length);
    $('#form-length-input').val(length);
    $('#form-square').text(square);
    $('#form-square-input').val(square);
    $('#form-cost').text(totalPrice);
    $('#form-cost-input').val(totalPrice);
  }

  $(document).ready(function () {
    $('.minus').click(function () {
      var min = parseFloat($('#width :selected').attr('data-default-len')) || 0.5;
      var $inputPS = $(this).parent().find('input.counter');
      var countPS = (parseFloat($inputPS.val()) - 0.1).toFixed(1);
      countPS = countPS < min ? min : countPS;
      if (isNaN(countPS)) {
        countPS = min;
      }
      $inputPS.val(countPS);
      moveSpinerCounter();
      // return false;
    });
    $('.plus').click(function () {
      var $inputPP = $(this).parent().find('input.counter');

      var newStrc = (parseFloat($inputPP.val()) + 0.1).toFixed(1);
      if (isNaN(newStrc)) {
        newStrc = 0.1;
      }
      $inputPP.val(newStrc);
      moveSpinerCounter();
      // return false;
    });
  });


  // ^^ ввод количества товара

  function changeNum() {

    // получение количества

    var v = parseFloat($('#jsfc').val());

    if (isNaN(v) || v < 1)

      return;

    // получение базовой цены

    var b = parseFloat($('#jsbase').text());

    if (isNaN(b) || b < 1)

      return;

    // запись новой цены

    var total = (v * b).toFixed(1);

    $('#jscost').text(total + ' грн');

  }

  $(document).on('keyup', '#jsfc', changeNum);
  $(document).on('change', '#jsfc', changeNum);


})(jQuery);


/*!

 * jQuery numberMask Plugin v1.0.0

 *

 * Licensed under the MIT License

 * Authors: Konstantin Krivlenia

 *          krivlenia@gmail.com

 * Site:  https://github.com/Mavrin/maskInput

 */

!function (e) {
  if ("function" == typeof define && define.amd) define(["jquery"], e);
  else if ("object" == typeof module && module.exports) {
    var t = require("jquery");
    module.exports = e(t)
  } else e(jQuery)
}(function (e) {
  return e.fn.numberMask = function (t) {
    var n, r = {
        type: "int",
        beforePoint: 10,
        afterPoint: 2,
        defaultValueInput: 0,
        allowNegative: !1,
        decimalMark: ["."],
        pattern: ""
      },
      a = function (e) {
        var t = e.which;
        if (e.ctrlKey || e.altKey || e.metaKey || 32 > t) return !0;
        if (t) {
          var a = String.fromCharCode(t),
            i = e.target.value,
            l = o(e.target);
          return i = i.substring(0, l.start) + a + i.substring(l.end), r.allowNegative && "-" === i || n.test(i)
        }
      },
      i = function (t) {
        var n = e(t.currentTarget);
        n.val(l(n))
      },
      o = function (e) {
        var t, n, r, a, i, o = 0,
          l = 0,
          c = !1;
        return "number" == typeof e.selectionStart && "number" == typeof e.selectionEnd ? (o = e.selectionStart, l = e.selectionEnd) : (n = document.selection.createRange(), n && n.parentElement() == e && (a = e.value.length, t = e.value.replace(/\r\n/g, "\n"), r = e.createTextRange(), r.moveToBookmark(n.getBookmark()), i = e.createTextRange(), i.collapse(!1), r.compareEndPoints("StartToEnd", i) > -1 ? o = l = a : (o = -r.moveStart("character", -a), o += t.slice(0, o).split("\n").length - 1, r.compareEndPoints("EndToEnd", i) > -1 ? l = a : (l = -r.moveEnd("character", -a), l += t.slice(0, l).split("\n").length - 1)))), o - l != 0 && (c = !0), {
          start: o,
          end: l,
          statusSelection: c
        }
      },
      l = function (e) {
        var t = e.val();
        return n.test(t) ? t : r.defaultValueInput
      },
      c = function () {
        for (var e = "(\\" + r.decimalMark[0], t = 1; t < r.decimalMark.length; t++) e += "|\\" + r.decimalMark[t];
        return e += ")"
      };
    if (this.bind("keypress", a).bind("input", i), t && (t.decimalMark && "string" === e.type(t.decimalMark) && (t.decimalMark = [t.decimalMark]), e.extend(r, t)), "object" == typeof r.pattern && r.pattern instanceof RegExp) n = r.pattern;
    else {
      var u = r.allowNegative ? "[-]?" : "",
        d = "^" + u + "\\d{1," + r.beforePoint + "}$",
        s = "^" + u + "\\d{1," + r.beforePoint + "}" + c() + "\\d{0," + r.afterPoint + "}$";
      n = new RegExp("int" == r.type ? d : d + "|" + s)
    }
    return this
  }, e
});

//# sourceMappingURL=jquery.numberMask.min.js.map
