<?php

/**
 * @file
 * Default theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct URL of the current node.
 * - $display_submitted: Whether submission information should be displayed.
 * - $submitted: Submission information created from $name and $date during
 *   template_preprocess_node().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type; for example, "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type; for example, story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode; for example, "full", "teaser".
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined; for example, $node->body becomes $body. When needing to
 * access a field's raw values, developers/themers are strongly encouraged to
 * use these variables. Otherwise they will have to explicitly specify the
 * desired field language; for example, $node->body['en'], thus overriding any
 * language negotiation rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 *
 * @ingroup themeable
 */
?>

<?php

hide($content['comments']);
hide($content['links']);
hide($content['field_shorttitle']);
hide($content['field_img']);
hide($content['field_price']);
hide($content['field_color']);
hide($content['field_features']);
hide($content['field_photo']);
hide($content['field_type']);
hide($content['field_shortstory']);
hide($content['field_characteristics']);
hide($content['field_faktura']);
hide($content['field_sklad']);
hide($content['field_new']);
hide($content['field_video']);

?>

<?php if ($page) { ?>
  <div class="card">

    <div class="primg">

      <?php if (arg(1) == 9) { ?>
        <div class="mainprodimg">

          <div class="tabs">
            <ul class="tabs__caption">
              <li class="active"><img src="<?php print base_path() . path_to_theme(); ?>/images/hf-green.png" border="0"
                                      alt="Green HF - зеленого цвета"/></li>
              <li><img src="<?php print base_path() . path_to_theme(); ?>/images/hf-blue.png" border="0"
                       alt="Green HF - синего цвета"/></li>
              <li><img src="<?php print base_path() . path_to_theme(); ?>/images/hf-red.png" border="0"
                       alt="Green HF - красного цвета"/></li>
              <li><img src="<?php print base_path() . path_to_theme(); ?>/images/hf-orange.png" border="0"
                       alt="Green HF - оранжевого цвета"/></li>
              <li><img src="<?php print base_path() . path_to_theme(); ?>/images/hf-pink.png" border="0"
                       alt="Green HF - розового цвета"/></li>
            </ul>

            <div class="tabs__content  active"><img
                src="<?php print base_path() . path_to_theme(); ?>/images/greenhf-green.jpg" border="0"
                alt="Green HF - зеленого цвета"/></div>
            <div class="tabs__content"><img src="<?php print base_path() . path_to_theme(); ?>/images/greenhf-blue.jpg"
                                            border="0" alt="Green HF - синего цвета"/></div>
            <div class="tabs__content"><img src="<?php print base_path() . path_to_theme(); ?>/images/greenhf-red.jpg"
                                            border="0" alt="Green HF - красного цвета"/></div>
            <div class="tabs__content"><img
                src="<?php print base_path() . path_to_theme(); ?>/images/greenhf-orange.jpg" border="0"
                alt="Green HF - оранжевого цвета"/></div>
            <div class="tabs__content"><img src="<?php print base_path() . path_to_theme(); ?>/images/greenhf-pink.jpg"
                                            border="0" alt="Green HF - розового цвета"/></div>

          </div>

        </div>
      <?php } else { ?>
        <div class="mainprodimg">

          <?php if (arg(1) == 16 || arg(1) == 17 || arg(1) == 18 || arg(1) == 101 || arg(1) == 102 || arg(1) == 49 || arg(1) == 58) { ?>
            <img src="<?php print base_path() . path_to_theme(); ?>/images/uv.png" border="0" alt="UV" class="itf"/><img
              src="<?php print base_path() . path_to_theme(); ?>/images/health-protect.png" border="0"
              alt="Health Protect" class="hp"/>
          <?php } elseif (arg(1) == 15) { ?>
            <img src="<?php print base_path() . path_to_theme(); ?>/images/itf.png" border="0" alt="ITF" class="itf"/>
          <?php } else { ?>
            <img src="<?php print base_path() . path_to_theme(); ?>/images/fifa.png" border="0" alt="FIFA рекомендует"
                 class="fifa"/>
          <?php } ?>

          <?php $new = field_get_items('node', $node, 'field_new');
          if ($new[0]['value'] == 1) { ?>
            <div class="new"><img src="<?php print base_path() . path_to_theme(); ?>/images/newicon.png" border="0"
                                  alt="Новинка"/></div><?php } ?>


          <?php $field3 = field_get_items('node', $node, 'field_img');
          if ($field3) {
            print l(theme('image', array('html' => TRUE, 'style_name' => '800', 'path' => $field3[0]['uri'], 'attributes' => array('class' => 'main-img'))), image_style_url('800', $field3[0]['uri']), array('html' => TRUE, 'attributes' => array('rel' => 'gallery', 'class' => 'fancybox')));
          } ?>

          <div class="smallimg">
            <?php
            if ($field3) {
              foreach (array_slice($field3, 1) as $key => $value) {
                print l(theme('image_style', array('style_name' => '80x60', 'path' => $value['uri'],)), image_style_url('800', $value['uri']), array('html' => TRUE, 'attributes' => array('data-fancybox' => 'gallery', 'class' => 'fancybox')));
              }
            }
            ?>
          </div>

        </div>
        <div class="faktura"><?php $field6 = field_get_items('node', $node, 'field_faktura');
          if ($field6) {
            print theme('image', array('path' => $field6[0]['uri']));
          } ?></div>
      <?php } ?>
    </div>

    <div class="buydiv">

      <?php $field = field_get_items('node', $node, 'field_price');
      if ($field) { ?>
        <div class="pgprice">
          <div class="pgcost"><span><?php print $field[0]['value']; ?></span>грн/м<sup>2</sup></div>
          <div id="jsbase" style="display:none"><?php print $field[0]['value']; ?></div>
        </div>
        <div class="optdiv">Для заказов более 100м<sup>2</sup> &mdash; <a href="#opt" class="fancyform">оптовая цена</a>
        </div>
      <?php } ?>

      <?php $field7 = field_get_items('node', $node, 'field_sklad');
      if ($field7) { ?>
        <?php if ($field7[0]['value'] != 0) { ?>
          <div class="pgbtn predzakaz"><a href="#order" class="fancyform">Сделать предзаказ</a></div>
          <div class="sklad off">под заказ</div>
        <?php } ?>
      <?php } ?>

      <div class="features">
        <?php $field1 = field_get_items('node', $node, 'field_color');
        if ($field1) { ?>
          <div class="feattl">Цвет</div>
          <div class="featxt"><?php print $field1[0]['value']; ?></div>
        <?php } ?>

        <?php $field2 = field_get_items('node', $node, 'field_features');
        if ($field2) { ?>
          <div class="feattl">Особенности</div>
          <div class="featxt"><?php print $field2[0]['value']; ?></div>
        <?php } ?>
      </div>

      <div class="proba"><a href="#sample" class="fancyform">Заказать образец</a></div>
    </div>

    <div class="clear"></div>
    <?php
          $onePrice = 0;
          if(isset($field[0]['value']) && $field[0]['value'] > 0){
              $onePrice = $field[0]['value'];
          } ?>
    <?php if ($field7[0]['value'] == 0 && $field[0]['value'] > 0) { ?>
      <div id="travaorder" style="display:none;">
        <form id="travasubmit">
          <div class="travawidth">
            <select name="width" id="width">
              <?php $width = field_get_items('node', $node, 'field_width');
              if ($width) {
                foreach ($width as $width_option) {
                  if ($width_option["value"] == 0) {
                    print '<option value="4m" data-default-len="' . round((($onePrice / 4) / $onePrice), 5) . '">4 метра</option>';
                  } elseif ($width_option["value"] == 1) {
                    print '<option value="3m" data-default-len="' .round((($onePrice / 3) / $onePrice), 5) .'">3 метра</option>';
                  } elseif ($width_option["value"] == 2) {
                    print '<option value="2m" data-default-len="' . round((($onePrice / 2) / $onePrice), 5) .'">2 метра</option>';
                  }
                }
              } ?>

            </select>
          </div>
          <div class="travaleght">
            <span id="travaleght_minus" class="minus"></span>
            <input type="text" id="length" name="length" class="counter" placeholder="Длина, м" value="<?php if ($onePrice) {
              echo(($onePrice / 4) / $onePrice);
            } ?>"/>
            <span id="travaleght_plus" class="plus"></span>
          </div>
          <div class="travaarea">= <span class="areanum">4</span> м<sup>2</sup></div>
          <div class="travaprice">Всего: <span class="pricenum"><?php if ($onePrice) {
                echo($onePrice * 4);
              } ?></span> грн
          </div>
          <div class="travabtn" id="order-bth"><a href="#order" class="fancyform">Заказать сейчас</a></div>
        </form>
      </div>
    <?php } ?>


    <div class="clear"></div>

    <?php $field5 = field_get_items('node', $node, 'field_shortstory');
    if ($field5) { ?>
      <div class="cardtxt"><p><?php print $field5[0]['value']; ?></p></div>
    <?php } ?>

  </div>

  <div class="clear"></div>

  <?php $field8 = field_get_items('node', $node, 'field_characteristics');
  if ($field8) { ?>
    <h3>Характеристики</h3>
  <?php } ?>

  <?php print render($content['field_characteristics']); ?>

  <div class="pgdesc">

    <?php print render($content['body']); ?>

    <div class="clear"></div>

    <div id="video_wrapper"><?php print render($content['field_video']); ?></div>

    <div class="photodiv">
      <h3>Применение</h3>

      <div class="pdlist">
        <ul>
          <?php
          $node_image = $node->field_photo['und'];

          foreach ($node_image as $node_image) {

            print   '<li>' . l(
                theme('image_style', array('style_name' => 'medium', 'path' => $node_image['uri'],)),
                image_style_url('800', $node_image['uri']),
                array('html' => TRUE, 'attributes' => array('class' => 'fancybox', 'data-fancybox' => 'gallery2'))) . '</li>';

          }
          ?>
        </ul>
      </div>
    </div>

    <div class="clear"></div>
    <div class="pgpointer"></div>

  </div>


  <div id="order" class="formavopros-wrap" style="display:none; width:600px;">
    <div id="before_click" class="before_click">
      <p class="popup-form-title">Оформить заявку</p>
      <div class="zakazproduct"><?php print render($title); ?></div>
      <form id="formavopros">
        <input type="hidden" value="<?php print render($title); ?>" name="title"/>
        <div class="in"><input type="text" name="name" placeholder="ФИО" class="intxt"/></div>
        <div class="in"><input type="text" name="phone" placeholder="Контактный номер телефона" id="phone2"
                               class="intxt"/></div>

        <!--	<div class="in">
			<p><strong>Количество (м<sup>2</sup>)</strong></p>
			<input type="text" id="jsfc" class="intxt" placeholder="Количество" value="1.0" name="count">
			<span>Стоимость:</span>
			<span id="jscost"><strong><?php print $field[0]['value']; ?> грн.</strong></span>
		</div>
	-->
        <div class="in" id="order-items" style="display:none;">
          <p>
            Ширина: <span id="form-width"></span> м.
            <input type="text" name="width" id="form-width-input" hidden>
          </p>
          <p>
            Длина: <span id="form-length"></span> м.
            <input type="text" name="length" id="form-length-input" hidden>
          </p>
          <p>
            Площадь: <span id="form-square"></span> кв.м.
            <input type="text" name="square" id="form-square-input" hidden>
          </p>
          <p>
            Стоимость: <span id="form-cost"></span> грн.
            <input type="text" name="cost" id="form-cost-input" hidden>
          </p>
        </div>
        <!-- Доставка и Оплата START	-->
        <div class="in" id="delpay">
          <p><strong>Доставка</strong></p>
          <div>
            <input type="radio" name="delivery" value="novaposhta">
            <span>Новая почта</span>
            <div data-delpay="novaposhta" style="display: none">
              <div>
                <input type="text" name="delivery_city" class="intxt" placeholder="Город">
              </div>
              <div>
                <input type="text" name="delivery_address" class="intxt" placeholder="Адрес или Номер отделения">
              </div>
            </div>
          </div>
          <div>
            <input type="radio" name="delivery" value="pickup">
            <span>Самовывоз</span>
            <div data-delpay="pickup" style="display: none">
                г.Кривой Рог, ул.Лётчиков 9 (бывший Атлант-Сервис)
            </div>
          </div>
          <p><strong>Оплата</strong></p>
          <div>
            <input type="radio" name="payment" value="privat24">
            <span>Privat 24</span>
            <div data-delpay="privat24" style="display: none">
                <i>Номер 5168 7427 1848 9382 Маслов Д.И.</i>
            </div>
          </div>
          <div>
            <input type="radio" name="payment" value="bank">
            <span>Оплата по реквизитам банка по платежному документу счет-фактуре</span>
            <div data-delpay="bank" style="display: none">
                <i>После подтверждения заказа оператором, мы выставляем вам счет по SMS, на email или Viber. Вы сможете оплатить его через онлайн банкинг или через кассу любого банка.</i>
            </div>
          </div>
          <div>
            <input type="radio" name="payment" value="cash">
            <span>Наличными</span>
            <div data-delpay="cash" style="display: none">
                <i>При самовывозе со склада в г. Кривом Роге, либо в магазине в г. Киеве</i>
            </div>
          </div>
          <div>
            <input type="radio" name="payment" value="nds">
            <span>Оплата по счету с НДС</span>
            <div data-delpay="nds" style="display: none">
                <i>Для выставления счета, просим указать код ЕГРПОУ вашей компании и скинуть на почту <a
                    href="mailto:info@ccgrass.com.ua">info@ccgrass.com.ua</a> регистрационные документы. После внесения
                  в базу мы сможем выставить вам счет с НДС.</i>
            </div>
          </div>
        </div>
        <!-- Доставка и Оплата END	-->
        <input type="hidden" id="order-input" name="order-input">
        <div class="in"><textarea name="vopros" placeholder="Укажите детали заявки" class="intxt"></textarea></div>
        <div class="in"><input type="submit" value="Отправить заявку" class="sbtn"/></div>
        <div class="clear twenty"></div>
      </form>
    </div>
    <div id="after_click" class="after_click" style="display:none;">
      <p class="resttl">Спасибо за ваше обращение!</p>
      <p>Мы свяжемся с вами в ближайшее время.</p>
    </div>
  </div>

  <div id="opt" style="display:none; width:600px;">
    <div class="opthead">Хотите заказать более 100м<sup>2</sup>?</div>
    <p>У нас есть для Вас специальные цены и условия! Пожалуйста обратитесь к нам для уточнения точной стоимости.</p>
    <div class="optnumber"><a href="tel:0679119090">(067) 911-90-90</a>,&nbsp;
      <a href="tel:0503217522">(050) 321-75-22</a>,&nbsp;<a href="tel:0564104505">(056) 410-45-05</a></div>
    <div class="optmail"><a class="fancyform" href="#request">info@ccgrass.com.ua</a></div>
  </div>

<?php } else { ?>

  <div class="simurl"><a href="<?php print $node_url; ?>"><?php print render($content['field_shorttitle']); ?></a></div>

  <?php $field7 = field_get_items('node', $node, 'field_sklad');
  if ($field7) { ?>
    <?php if ($field7[0]['value'] == 0) { ?>
      <div class="sklad on">в наличии</div>
    <?php } else { ?>
      <div class="sklad off">под заказ</div>
    <?php } ?>
  <?php } ?>
  <?php $field = field_get_items('node', $node, 'field_price');
  if ($field) { ?>
    <?php if (!empty($field[0]['value'])) { ?>
      <div class="cost"><span><?php print round($field[0]['value']); ?></span> грн/м<sup>2</sup></div>
    <?php } ?>
  <?php } ?>


  <?php $new = field_get_items('node', $node, 'field_new');
  if ($new[0]['value'] == 1) { ?>
    <div class="newteaser"><img src="<?php print base_path() . path_to_theme(); ?>/images/newicon.png" border="0"
                                alt="Новинка"/></div><?php } ?>

  <a
    href="<?php print $node_url; ?>"><?php print theme('image_style', array('style_name' => '278x145', 'path' => $node->field_img[LANGUAGE_NONE][0]['uri'], 'class' => 'preview')); ?></a>


<?php } ?>
