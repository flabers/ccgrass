<div class="toppage">
    <div id="wrap" class="toppage-mobile">
        <div class="logo"><a href="<?php print $front_page; ?>"><img src="<?php print base_path().path_to_theme(); ?>/images/logo.png" /></a></div>
        <div class="topmenu">
            <a href="javascript:void(0)" class="topmenu-mobile-btn">
                <span class="mm-line"></span>
            </a>
            <?php print render($page['menu']); ?>
        </div>
        <div class="topmain-right">
            <div class="change-sity">
                <select name="sity" id="sity" class="change-sity__select">
                    <option value="krivoy_rog">Кривой Рог</option>
                    <option value="kyiv">Киев</option>
                </select>
            </div>
            <a href="#phonesWrap" class="hd-phone-icon fancyphones"></a>
            <div class="toptel" id="phonesWrap">
                <div class="hd-phone hd-phone_sity-change" data-sity="krivoy_rog">
                    <a href="tel:0679119090" class="maintel"><span>(067)</span> 911-90-90</a>
                    <div class="hovertel">
                        <a href="tel:0503217522"><span>(050)</span> 321-75-22</a>
                        <a href="tel:0564990299"><span>(056)</span> 499-02-99</a>
                        <div class="recall"><a href="#request" class="fancyform"><span>Перезвонить вам?</span></a></div>
                    </div>
                </div>
                <div  class="hd-phone hd-phone_sity-change" data-sity="kyiv">
                    <a href="tel:0981662686" class="maintel"><span>(098)</span> 166 26 86</a>
                    <div class="hovertel">
                        <a href="tel:0443904441"><span>(044)</span> 390 44 41</a>
                        <div class="recall"><a href="#request" class="fancyform"><span>Перезвонить вам?</span></a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="clear"></div>
</div>
<!-- Facebook Pixel Code -->
<script>
    !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
        n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
        document,'script','https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '346173409285559'); // Insert your pixel ID here.
    fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
               src="https://www.facebook.com/tr?id=346173409285559&ev=PageView&noscript=1"
    /></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->
<?php if (arg(1) == 8) { ?>
    <div id="contactpage">
        <div class="conttxt"><?php print render($page['content']); ?></div>
        <div id="map">
            <script>
                function getCookie(name) {
                    var matches = document.cookie.match(new RegExp(
                        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
                    ));
                    return matches ? decodeURIComponent(matches[1]) : undefined;
                }
                document.addEventListener('DOMContentLoaded', function() {
                if(getCookie("sity")=="kyiv"){
                    console.log("if");
                    document.getElementById('container-map').innerHTML = '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1270.7007959266937!2d30.477728772079608!3d50.433619996371306!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zNTDCsDI2JzAxLjAiTiAzMMKwMjgnNDMuMSJF!5e0!3m2!1suk!2sua!4v1616497046432!5m2!1suk!2sua" height="450" style="border:0;width:100%;" allowfullscreen="" loading="lazy"></iframe>'
                    // function initMap() {
                    //     var uluru = {lat: 50.433731, lng: 30.478703,};
                    //     var map = new google.maps.Map(document.getElementById('map'), {
                    //         zoom: 12,
                    //         center: uluru
                    //     });
                    //     var marker = new google.maps.Marker({
                    //         position: uluru,
                    //         map: map
                    //     });
                    // }
                }else {
                    console.log("else");
                    document.getElementById('container-map').innerHTML = '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1336.506403163452!2d33.3936276583394!3d47.936135994801965!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zNDfCsDU2JzEwLjEiTiAzM8KwMjMnNDEuMCJF!5e0!3m2!1suk!2sua!4v1616497178846!5m2!1suk!2sua" height="450" style="border:0;width:100%;" allowfullscreen="" loading="lazy"></iframe>'
                    
                    // function initMap() {
                    //     var uluru = {lat: 47.9356426, lng: 33.3950913,};
                    //     var map = new google.maps.Map(document.getElementById('map'), {
                    //         zoom: 12,
                    //         center: uluru
                    //     });
                    //     var marker = new google.maps.Marker({
                    //         position: uluru,
                    //         map: map
                    //     });
                    // }
                }
            });
                // $(document).ready(function () {
                //     $("body").on('DOMSubtreeModified', ".nice-select",function () {
                //         document.location.reload(true);
                //     });
                // });
            </script>
            <div id="container-map" class="container-map"></div>
        </div>
    </div>
<?php } else { ?>
    <div id="wrap">
        <div class="pgcontent">
            <div class="pgright">
                <div class="krohi">
                    <?php print $breadcrumb; ?>
                </div>
                <?php print $messages; ?>
                <div class="clearfix" id="tab"><?php if ($tabs): ?><?php print render($tabs); ?><?php endif; ?></div>
                <?php if ($action_links): ?><ul class="action-links"><?php print render($action_links); ?></ul><?php endif; ?>
                <div class="clear"></div>

                <?php if (arg(1) != 4) { ?>
                    <div class="pgttl"><h1><?php print $title ?></h1></div>
                <?php } ?>

                <div class="pagetext"><?php print render($page['content']); ?></div></div>
            <div class="pgleft">
                <?php print render($page['pageleft']); ?>
            </div>
        </div>
    </div>
<?php } ?>
<div class="clear"></div>
<div id="form">
    <div id="wrap">
        <div class="formttl"><a href="#sample" class="fancyform">Заказать бесплатные образцы</a></div>
        <div class="formdiv">
            <div class="inputs" id="orazform">
                <form id="orazets">
                    <input name="name" class="nameinput" placeholder="Введите свое имя" />
                    <input name="phone" class="phoneinput" placeholder="Контактный телефон" id="phone" />
                    <input type="submit" value="Перезвонить" class="btn" />
                </form>
            </div>
            <div id="orazresult">
                Ваша заявка успешно отправлена! Ождайте нашего звонка.
            </div>
            <div class="formtxt">или звоните</div>
            <div class="formphone formphone-sity-change" data-sity="krivoy_rog"><a href="tel:0679119090">(067) 911-90-90</a>,&nbsp;<a href="tel:0503217522">(050) 321-75-22</a></div>
            <div class="formphone formphone-sity-change" data-sity="kyiv"><a href="tel:0443904441">(044) 390 44 41</a>,&nbsp;<a href="tel:0981662686">(098) 166 26 86</a></div>
        </div>
    </div>
</div>
<div class="clear"></div>
<div id="wrap">
    <?php print render($page['products']); ?>
</div>
<div class="clear"></div>
<div id="footer">
    <div id="wrap">
        <div class="topfooter">
            <div class="fmenu">
                <?php print render($page['footermenu']); ?>
            </div>
            <div class="fphone fphone_sity-change" data-sity="krivoy_rog"><a href="tel:0679119090">(067) 911-90-90</a>,&nbsp;
                <a href="tel:0503217522">(050) 321-75-22</a>,&nbsp;<a href="tel:0564990299">(056) 499-02-99</a></div>
            <div class="fphone fphone_sity-change" data-sity="kyiv"><a href="tel:0443904441">(044) 390 44 41</a>,&nbsp;<a
                        href="tel:0981662686">(098) 166 26 86</a></div>
            <div class="clear"></div>
        </div>
        <div class="botfooter">
            <div class="sitename">CCGrass Украина (с) 2016</div>
            <div class="socbtns"><a href="#"><img src="<?php print base_path().path_to_theme(); ?>/images/soc.png" alt="" /></a></div>
            <div class="copyright">Создание сайта - <a href="https://flabers.com/" target="_blank">Интернет-агентство Флаберс</a></div>
        </div>
    </div>
</div>