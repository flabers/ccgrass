<?php

header('Content-Type: text/html; charset=utf-8');

$name=isset($_POST["name"]) ? formatFormData($_POST["name"]) : '';
$phone=isset($_POST["phone"]) ? formatFormData($_POST["phone"]) : '';
$email=isset($_POST["email"]) ? formatFormData($_POST["email"]) : '';
$mess=isset($_POST["vopros"]) ? formatFormData($_POST["vopros"]) : '';
$title=isset($_POST["title"]) ? formatFormData($_POST["title"]) : '';
$payment=isset($_POST["payment"]) ? formatFormData($_POST["payment"]) : '';
$delivery=isset($_POST["delivery"]) ? formatFormData($_POST["delivery"]) : '';
$delivery_city=isset($_POST["delivery_city"]) ? formatFormData($_POST["delivery_city"]) : '';
$delivery_address=isset($_POST["delivery_address"]) ? formatFormData($_POST["delivery_address"]) : '';

$width=isset($_POST["width"]) ? number_format((float)$_POST["width"], 0, '.', '') : '0';
$length=isset($_POST["length"]) ? number_format((float)$_POST["length"], 1, '.', '')  : '0';
$square=isset($_POST["square"]) ? number_format((float)$_POST["square"], 1, '.', '') : '0';
$cost=isset($_POST["cost"]) ? number_format((float)$_POST["cost"], 1, '.', '') : '0';

if(!empty($_POST['order-input'])) { $order=formatFormData($_POST["order-input"]); }

// $to = "office@enita.com.ua, info@ccgrass.com.ua, iflabers@mail.ru";
$subject = "Заказ с сайта $title";
// $headers = "From: CCGrass\r\n";
// $headers .= "Content-type: text/html; charset=\"utf-8\"";


$payment_html = '<p>Способ оплаты: ';

switch ($payment){
  case 'privat24':
    $payment_html .= 'Приват 24</p>';
    break;
  case 'cash':
    $payment_html .= 'Наличная оплата</p>';
    break;
  case 'bank':
    $payment_html .= 'Оплата по реквизитам банка по платежному документу счет-фактуре</p>';
    break;
  case 'nds':
    $payment_html .= 'Оплата по счету с НДС</p>';
    break;
  default:
    $payment_html .= '</p>';
}


$delivery_html = '<p>Способ доставки: ';

switch ($delivery){
  case 'novaposhta':
    $delivery_html .= 'Новая почта</p>';
    $delivery_html .= "\n<p>Город: " . $delivery_city . "</p>";
    $delivery_html .= "\n<p>Адрес или Номер отделения: " . $delivery_address . "</p>";
    break;
  case 'pickup':
    $delivery_html .= 'Самовывоз</p>';
    break;
  default:
    $delivery_html .= '</p>';
}


$message = "

<h2>Заявка:</h2>

<p><strong>$title</strong></p>

<p>ФИО: $name</p>
<p>Телефон: $phone</p>
<p>Ширина: $width м.</p>
<p>Длина: $length м.</p>
<p>Площадь: $square кв.м</p>
<p>Стоимость: $cost грн.</p>
$delivery_html
$payment_html
<p>Комментарий: $mess</p>


$order
";
//mail($to,$subject,$message,$headers);

sendBySparkPost("sales@enita.com.ua", $subject, $message);
//sendBySparkPost("info@ccgrass.com.ua", $subject, $message);
sendBySparkPost("iflabers@mail.ru", $subject, $message);

function sendBySparkPost($recepient, $header, $html) {
    if ($recepient == "info@ccgrass.com.ua" || $html == false || $header == false)
        return;

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL,"https://api.sparkpost.com/api/v1/transmissions");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Authorization: 3f790cb93cac9983d79bd330b47fe7af69efa37e',
        'Content-Type: application/json'
    ));

    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode(
        array("content"=>array(
            "from"=>"info@ccgrass.com.ua",
            "subject"=>$header,
            "html"=>$html),
            "recipients" => array(array("address" => $recepient))
        )));
    $server_output = curl_exec ($ch);
    curl_close ($ch);
}

function formatFormData($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}

?>
