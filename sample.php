<?php

header('Content-Type: text/html; charset=utf-8');

$name = isset($_POST["name"]) ? formatFormData($_POST["name"]) : '';
$phone = isset($_POST["phone"]) ? formatFormData($_POST["phone"]) : '';
$mess = isset($_POST["vopros"]) ? formatFormData($_POST["vopros"]) : '';

$samples = isset($_POST["samples"]) ? (array)$_POST["samples"] : array();

$delivery = isset($_POST["delivery"]) ? formatFormData($_POST["delivery"]) : '';
$delivery_city = isset($_POST["delivery_city"]) ? formatFormData($_POST["delivery_city"]) : '';
$delivery_address = isset($_POST["delivery_address"]) ? formatFormData($_POST["delivery_address"]) : '';

// $to = "danila_m@mail.ru, iflabers@mail.ru";
$subject = "Заказ образца";
// $headers = "From: CCGrass\r\n";
// $headers .= "Content-type: text/html; charset=\"utf-8\"";

$samples_str = '';

foreach ($samples as $sample) {
  if (empty($sample)) {
    continue;
  }
  if (empty($samples_str) === false) {
    $samples_str .= ',';
  }
  $samples_str .= ' ' . formatFormData($sample);
}

$subject .= $samples_str;

$delivery_html = '<p>Способ доставки: ';

switch ($delivery) {
  case 'novaposhta':
    $delivery_html .= 'Новая почта</p>';
    $delivery_html .= "\n<p>Город: " . $delivery_city . "</p>";
    $delivery_html .= "\n<p>Адрес или Номер отделения: " . $delivery_address . "</p>";
    break;
  case 'pickup':
    $delivery_html .= 'Самовывоз</p>';
    break;
  default:
    $delivery_html .= '</p>';
}

$message = "

<h2>Заказ образца $samples_str:</h2>

<p>ФИО: $name</p>
<p>Телефон: $phone</p>
$delivery_html
<p>Образцы: $samples_str</p>
<p>Комментарий: $mess</p>

";
//mail($to,$subject,$message,$headers);

sendBySparkPost("sales@enita.com.ua", $subject, $message);
//sendBySparkPost("info@ccgrass.com.ua", $subject, $message);
sendBySparkPost("iflabers@mail.ru", $subject, $message);

function sendBySparkPost($recepient, $header, $html)
{
  if ($recepient == "info@ccgrass.com.ua" || $html == false || $header == false)
    return;

  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, "https://api.sparkpost.com/api/v1/transmissions");
  curl_setopt($ch, CURLOPT_POST, 1);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

  curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'Authorization: 3f790cb93cac9983d79bd330b47fe7af69efa37e',
    'Content-Type: application/json'
  ));

  curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode(
    array("content" => array(
      "from" => "info@ccgrass.com.ua",
      "subject" => $header,
      "html" => $html),
      "recipients" => array(array("address" => $recepient))
    )));
  $server_output = curl_exec($ch);
  curl_close($ch);
}


function formatFormData($data)
{
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}

?>
